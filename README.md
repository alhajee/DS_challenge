# DS Challenge for Monitaur

## Goal
Build a basic classification model using a Scikit Learn pipeline and deploy via a REST API

## Format Requirements
- Fork or clone this repo
- Maintain the directory structure given for the project
- Use Python 3.7+
- If you need additional imports specify them in `requirements.txt`

## Model Requirements
- Model should be in the form of a [scikit-learn pipeline](https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.Pipeline.html)
- Create a model artifact using Pickle or Joblib

<!-- - Achieve a model RMSE less than or equal to 2.551 against some withheld test set -->

## API Requirements
- Serve the model as a REST API using Flask or FastAPI
- Be able to use CURL to send in feature inputs and return the prediction.
<!-- - Output must conform to the format given in the `Test Case` -->

## Data
The dataset you will be using contains information about Singapore Automobile Claims. You will be predicting the `Clm_Count`, you may generate non-Integer predictions.

[Source](https://instruction.bus.wisc.edu/jfrees/jfreesbooks/Regression%20Modeling/BookWebDec2010/CSVData/SingaporeAuto.csv)

[Format](https://rdrr.io/cran/insuranceData/man/SingaporeAuto.html)

[Data Description (see Page 21)](https://instruction.bus.wisc.edu/jfrees/jfreesbooks/Regression%20Modeling/BookWebDec2010/DataDescriptions.pdf)

<!-- ## Test Case
Run the following curl command against your model to verify that your model works as we expect:

```
TODO: Provide CURL command and output format
``` -->

## Submission
Timebox this challenge to 3 hours. After completing the assignment, you will meet with the data science team to demo your results and share your code.
